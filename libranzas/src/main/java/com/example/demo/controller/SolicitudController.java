package com.example.demo.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.interfacesService.ISolicitudService;
import com.example.demo.model.Respuesta;
import com.example.demo.model.Solicitud;

@RestController
@RequestMapping
public class SolicitudController {

	private static final String SOLICITUD_CONSULTADA = "Solicitud consultada";
	private static final String SOLICITUD_ACTUALIZADA_EXITOSAMENTE = "Solicitud actualizada exitosamente";
	private static final String SOLICITUD_GUARDADA_EXITOSAMENTE = "Solicitud guardada exitosamente";
	private static final String SOLICITUDES_LISTADAS_EXITOSAMENTE = "Solicitudes listadas exitosamente";
	private static final String SOLICITUD_ELIMINADA_EXITOSAMENTE = "Solicitud eliminada exitosamente";
	@Autowired
	private ISolicitudService solicitudService;

	@GetMapping("/solicitudes/listar")
	public Respuesta<List<Solicitud>> listar() {
		Respuesta<List<Solicitud>> respuesta = new Respuesta<List<Solicitud>>();
		try {
			List<Solicitud> solicitudes = solicitudService.listar();
			return respuesta.ok(solicitudes, SOLICITUDES_LISTADAS_EXITOSAMENTE);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}

	@PostMapping("solicitudes/guardar")
	public Respuesta<Integer> guardar(@RequestBody Solicitud solicitud) {
		Respuesta<Integer> respuesta = new Respuesta<Integer>();
		try {
			Integer id = solicitudService.guardar(solicitud);
			return respuesta.ok(id, SOLICITUD_GUARDADA_EXITOSAMENTE);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}

	@DeleteMapping("/solicitudes/eliminar/{id}")
	public Respuesta<Solicitud> eliminar(@PathVariable("id") int id) {
		Respuesta<Solicitud> respuesta = new Respuesta<Solicitud>();
		try {
			solicitudService.eliminar(id);
			return respuesta.ok(null, SOLICITUD_ELIMINADA_EXITOSAMENTE);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}

	@PutMapping("/solicitudes/actualizar/{id}")
	public Respuesta<Solicitud> actualizar(@PathVariable("id") int id, @RequestBody Solicitud solicitud) {
		Respuesta<Solicitud> respuesta = new Respuesta<Solicitud>();
		try {
			solicitudService.actualizar(id, solicitud);
			return respuesta.ok(null, SOLICITUD_ACTUALIZADA_EXITOSAMENTE);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}
	
	@GetMapping("/solicitudes/detalles/{id}")
	public Respuesta<Solicitud> buscar(@PathVariable("id") int id) {
		Respuesta<Solicitud> respuesta = new Respuesta<Solicitud>();
		try {
			Solicitud solicitud = solicitudService.listarId(id);
			return respuesta.ok(solicitud, SOLICITUD_CONSULTADA);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}
	
	@GetMapping("/solicitudes/calcularMonto")
	public Respuesta<Solicitud> calcularMonto(@RequestBody List<LocalDate> fechas) {
		Respuesta<Solicitud> respuesta = new Respuesta<Solicitud>();
		try {
			return respuesta.ok(null, SOLICITUD_CONSULTADA);
		} catch (Exception e) {
			return respuesta.error(e.getMessage());
		}
	}
}
