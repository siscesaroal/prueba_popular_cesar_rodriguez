package com.example.demo.model;

public class Respuesta<T> {
	
	private boolean success;
	
	private T data;
	
	private String message;

	public Respuesta() {
		super();
	}

	public Respuesta(boolean success, T data, String message) {
		super();
		this.success = success;
		this.data = data;
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Respuesta<T> ok(T informacion, String mensaje) {
		Respuesta<T> respuesta = new Respuesta<>();
		respuesta.setSuccess(true);
		respuesta.setData(informacion);
		respuesta.setMessage(mensaje);
		return respuesta;
	}
	
	public Respuesta<T> error(String mensaje) {
		Respuesta<T> respuesta = new Respuesta<>();
		respuesta.setSuccess(false);
		respuesta.setData(null);
		respuesta.setMessage(mensaje);
		return respuesta;
	}
}
