package com.example.demo.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T01_SOLICITUD")
public class Solicitud {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "ESTADO")
	private Integer estado;

	@Column(name = "FECHA_INGRESO")
	private LocalDate fechaIngreso;

	@Column(name = "MONTO")
	private Double monto;

	@Column(name = "ID_CLIENTE", unique = true)
	private String idCliente;

	public Solicitud() {
		super();
	}

	public Solicitud(int id, Integer estado, LocalDate fechaIngreso, Double monto, String idCliente) {
		super();
		this.id = id;
		this.estado = estado;
		this.fechaIngreso = fechaIngreso;
		this.monto = monto;
		this.idCliente = idCliente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
}
