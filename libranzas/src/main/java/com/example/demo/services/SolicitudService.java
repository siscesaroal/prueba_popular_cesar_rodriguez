package com.example.demo.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.example.demo.interfaces.ISolicitud;
import com.example.demo.interfacesService.ISolicitudService;
import com.example.demo.model.Solicitud;

@Service
public class SolicitudService implements ISolicitudService {

	private static final String LA_SOLICITUD_A_ACTUALIZAR_NO_EXISTE = "La solicitud a actualizar no existe";

	private static final String LA_SOLICITUD_A_ELIMINAR_NO_EXISTE = "La solicitud a eliminar no existe";

	private static final String LA_SOLICITUD_A_BUSCADA_NO_EXISTE = "La solicitud a buscada no existe";

	private static final String LA_FECHA_DE_INGRESO_DEBE_SER_POSTERIOR_A_LA_FECHA_ACTUAL = "La fecha de ingreso debe ser posterior a la fecha actual";
	
	private static final String EL_MONTO_DEBE_SER_SUPERIOR_A_1000000 = "El monto debe ser superior a 1000000";
	
	private static final String NO_HAY_SOLICITUD_PARA_GUARDAR = "No hay solicitud para guardar";
	
	@Autowired
	private ISolicitud data;

	@Override
	public List<Solicitud> listar() {
		return (List<Solicitud>) data.findAll();
	}

	@Override
	public Solicitud listarId(int id) throws Exception {
		Optional<Solicitud> solicitud = data.findById(id);
		if (solicitud.isPresent()) {
			return solicitud.get();			
		}
		throw new Exception(LA_SOLICITUD_A_BUSCADA_NO_EXISTE);
	}

	@Override
	public int guardar(Solicitud solicitud) throws Exception {
		validarSolicitud(solicitud);
		Solicitud solicitudGuardada = data.save(solicitud);
		return solicitudGuardada.getId();
	}

	@Override
	public void eliminar(int id) throws Exception {
		Optional<Solicitud> solicitud = data.findById(id);
		if (solicitud.isPresent()) {
			data.delete(solicitud.get());			
		} else {
			throw new Exception(LA_SOLICITUD_A_ELIMINAR_NO_EXISTE);
		}
	}

	@Override
	public void actualizar(int id, Solicitud solicitud) throws Exception {
		Optional<Solicitud> opcional = data.findById(id);
		if (opcional.isPresent()) {
			Solicitud solicitudActualizar = opcional.get();
			validarSolicitud(solicitud);
			solicitudActualizar.setEstado(solicitud.getEstado());
			solicitudActualizar.setFechaIngreso(solicitud.getFechaIngreso());
			solicitudActualizar.setMonto(solicitud.getMonto());
			solicitudActualizar.setIdCliente(solicitud.getIdCliente());
			data.save(solicitudActualizar);			
		} else {
			throw new Exception(LA_SOLICITUD_A_ACTUALIZAR_NO_EXISTE);
		}
	}
	
	private void validarSolicitud(Solicitud solicitud) throws Exception {
		if (Objects.nonNull(solicitud)) {
			if (Objects.nonNull(solicitud.getMonto()) && solicitud.getMonto() < 1000000) {
				throw new Exception(EL_MONTO_DEBE_SER_SUPERIOR_A_1000000);
			}
			if (Objects.nonNull(solicitud.getFechaIngreso()) && solicitud.getFechaIngreso().isBefore(LocalDate.now())) {
				throw new Exception(LA_FECHA_DE_INGRESO_DEBE_SER_POSTERIOR_A_LA_FECHA_ACTUAL);
			}
		} else {
			throw new Exception(NO_HAY_SOLICITUD_PARA_GUARDAR);
		}
	}

	@Override
	public String calcularMonto(List<LocalDate> fechas) throws Exception {
		
		return null;
	}
}
