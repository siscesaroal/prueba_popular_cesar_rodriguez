package com.example.demo.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Solicitud;

@Repository
public interface SolicitudRepository extends JpaRepository<Solicitud, Integer> {
	
	@Query("SELECT s FROM Solicitud s WHERE s.fechaIngreso = :fechaIngreso")
	public List<Solicitud> buscarFechaIngreso(LocalDate fechaIngreso);
	
}
