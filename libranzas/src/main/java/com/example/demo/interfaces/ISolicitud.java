package com.example.demo.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Solicitud;

@Repository
public interface ISolicitud extends CrudRepository<Solicitud, Integer> {
}
