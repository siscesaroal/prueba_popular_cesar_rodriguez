package com.example.demo.interfacesService;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.model.Solicitud;

public interface ISolicitudService {

	public List<Solicitud> listar();

	public Solicitud listarId(int id) throws Exception;

	public int guardar(Solicitud solicitud) throws Exception;

	public void eliminar(int id) throws Exception;

	public void actualizar(int id, Solicitud solicitud) throws Exception;
	
	public String calcularMonto(List<LocalDate> fechas) throws Exception;
}
