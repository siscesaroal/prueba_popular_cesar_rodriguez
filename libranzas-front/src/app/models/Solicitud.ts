export class Solicitud {
  constructor(
    public id: number,
    public estado: number,
    public fechaIngreso: string,
    public monto: number,
    public idCliente: string,
  ) {
  }
}
