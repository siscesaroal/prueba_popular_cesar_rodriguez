export class Respuesta<T>{
  constructor(
    public success: boolean = false,
    public data: T,
    public message: string = ''
  ) {
  }
}
