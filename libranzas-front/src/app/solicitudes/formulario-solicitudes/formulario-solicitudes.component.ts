import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../app.service';
import {Solicitud} from '../../models/Solicitud';

@Component({
  selector: 'app-formulario-solicitudes',
  templateUrl: './formulario-solicitudes.component.html',
  styleUrls: ['./formulario-solicitudes.component.css']
})
export class FormularioSolicitudesComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private appService: AppService
  ) {
    this.construirFormulario();
  }

  private construirFormulario(): void {
    this.formulario = new FormGroup({
      estado: new FormControl(null, Validators.required),
      fechaIngreso: new FormControl(null, Validators.required),
      monto: new FormControl(null, Validators.required),
      idCliente: new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
  }

  guardar(): void {
    if (this.formulario.valid) {
      this.appService.guardar(this.construirSolicitud())
        .subscribe(respuesta => {
          alert(respuesta.message);
        });
    } else {
      alert('Los campos son obligatorios');
    }
  }

  private construirSolicitud(): Solicitud {
    const solicitud: Solicitud = new Solicitud(
      null,
      this.formulario.get('estado').value,
      this.formulario.get('fechaIngreso').value,
      this.formulario.get('monto').value,
      this.formulario.get('idCliente').value,
    );
    return solicitud;
  }

}
