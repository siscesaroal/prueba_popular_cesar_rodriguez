import {Component, OnInit} from '@angular/core';
import {Solicitud} from '../models/Solicitud';
import {AppService} from '../app.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {

  solicitudes: Solicitud[] = [];

  constructor(
    private service: AppService,
    private router: Router,
  ) {
  }

  guardar(): void {
    this.router.navigate(['crear']);
  }

  ngOnInit(): void {
    this.obtenerSolicitudes();
  }

  private obtenerSolicitudes(): void {
    this.service.obtenerSolicitudes()
      .subscribe(respuesta => {
        this.solicitudes = respuesta.data;
      });
  }
}
