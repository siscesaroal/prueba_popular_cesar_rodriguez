import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormularioSolicitudesComponent} from './solicitudes/formulario-solicitudes/formulario-solicitudes.component';
import {SolicitudesComponent} from './solicitudes/solicitudes.component';

const routes: Routes = [
  {
    path: '',
    component: SolicitudesComponent
  },
  {
    path: 'crear',
    component: FormularioSolicitudesComponent,
    data: {tipoOperacion: 'crear'}
  },
  {
    path: 'editar',
    component: FormularioSolicitudesComponent,
    data: {tipoOperacion: 'editar'}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
