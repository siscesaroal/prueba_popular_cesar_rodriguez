import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Respuesta} from './models/Respuesta';
import {Solicitud} from './models/Solicitud';

@Injectable()
export class AppService {

  private urlSolicitudes = `${environment.urlServer}solicitudes/`;

  constructor(
    protected http: HttpClient,
  ) {
  }

  public obtenerSolicitudes(): Observable<Respuesta<Solicitud[]>> {
    return this.http.get<Respuesta<Solicitud[]>>(
      `${this.urlSolicitudes}listar`
    );
  }

  public guardar(solicitud: Solicitud): Observable<Respuesta<number>> {
    return this.http.post<Respuesta<number>>(
      `${this.urlSolicitudes}guardar`,
      solicitud
    );
  }
}
