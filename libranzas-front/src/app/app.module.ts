import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppService} from './app.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {SolicitudesComponent} from './solicitudes/solicitudes.component';
import {FormularioSolicitudesComponent} from './solicitudes/formulario-solicitudes/formulario-solicitudes.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    FormularioSolicitudesComponent,
    SolicitudesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpClient,
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
